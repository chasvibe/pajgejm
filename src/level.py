import pygame
import sys
import os
from config import *
from tile import Tile
from player import Player
from debug import debug
from support import *


class Level:
    def __init__(self):

        # Get display surface
        self.display_surface = pygame.display.get_surface()

        # Sprite groups
        self.visible_sprites = YSortCameraGroup()
        self.obstacle_sprites = pygame.sprite.Group()

        # Sprite setup
        self.create_map()

    def create_map(self):
        layouts = {
            'NOGO': open_csv("lands/csv/World_data_NOGO.csv"),
            'Detail': open_csv("lands/csv/Ground_Detail.csv"),
            'Structure': open_csv("lands/csv/World_data_STRUCTS.csv")
        }
        graphics = {
            'structs': import_folder("lands/environment/STRUCTS")
        }
        for style, layout in layouts.items():
            for index_y, row in enumerate(layout):
                for index_x, col in enumerate(row):
                    if col != '-1':
                        x = index_x * TILESIZE
                        y = index_y * TILESIZE
                        if style == 'NOGO':
                            Tile((x, y), [self.obstacle_sprites], 'invisible')
                        if style == 'Detail':
                            # Create detail
                            pass
                        if style == 'Structure':
                            surf = graphics['structs'][int(col)]
                            Tile((x, y), [self.visible_sprites, self.obstacle_sprites], 'Structure', surf)

        self.player = Player((SPAWN_POINT), [self.visible_sprites], self.obstacle_sprites)

    def run(self):
        # Update and draw game
        self.visible_sprites.custom_draw(self.player)
        self.visible_sprites.update()


class YSortCameraGroup(pygame.sprite.Group):
    def __init__(self):
        super().__init__()
        self.display_surface = pygame.display.get_surface()
        self.half_width = self.display_surface.get_size()[0] // 2
        self.half_height = self.display_surface.get_size()[1] // 2
        self.offset = pygame.math.Vector2()

        self.ground_surf = pygame.image.load('lands/images/World_data.png').convert()
        self.ground_rect = self.ground_surf.get_rect(topleft=(0, 0))

    def custom_draw(self, player):
        self.offset.x = player.rect.centerx - self.half_width
        self.offset.y = player.rect.centery - self.half_height

        ground_offset_pos = self.ground_rect.topleft - self.offset
        self.display_surface.blit(self.ground_surf, ground_offset_pos)

        for sprite in sorted(self.sprites(), key=lambda sprite: sprite.rect.centery):
            offset_pos = sprite.rect.topleft - self.offset
            self.display_surface.blit(sprite.image, offset_pos)
