from os import walk
from csv import reader
import pygame


def open_csv(path):
    terrain_map = []
    with open(path) as level_map:
        layout = reader(level_map, delimiter=',')
        for row in layout:
            terrain_map.append(list(row))
        return terrain_map


def import_folder(path):
    sorted_list = []
    surf_list = []

    for _, __, img_files in walk(path):
        for image in img_files:
            if image == '.DS_Store':
                continue
            full_path = path + '/' + image
            sorted_list.append(full_path)

    for img in sorted(sorted_list):
        image_surf = pygame.image.load(img).convert_alpha()
        surf_list.append(image_surf)

    return surf_list


if __name__ == '__main__':
    import_folder("lands/environment/STRUCTS")